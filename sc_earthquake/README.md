![Mastodon Follow](https://img.shields.io/mastodon/follow/000083616?domain=https%3A%2F%2Fmastodon.online&style=social)

# Earthquake Data Explorer

A data visualization app for the 2021 through 2022 South Carolina Earthquake Swarm.  The application will display a map 
with each event plotted.  You can hover the mouse pointer over a marker to display some information about the 
earthquake event at that location.  By default, when the app is launched, it displays the events starting on December 
2021 until the current date.  You can filter the events displayed by using the date filter and/or the earthquake 
magnitude filter.  To display detailed DYFI data about a particular event, click on the event's marker to select it, 
then choose a graph plot type from the dropdown.  The graph plot will be displayed below the events map.

This application was developed with Python, Dash, a library for creating low-code web data applications, and Plotly, the 
library used for creating the maps and table visualizations.

## Limitations
1) Only earthquake events that happen within bounding box:  max latitude = 35.261 & min latitude = 31.977, max longitude = -77.86 & min longitude = -83.485
2) The zipcode choropleth map only displays zipcodes within the state of South Carolina.  This is mainly a performance issue; As more zipcodes are added to
   the map, the slower it is to render.  Searching for a solution.

## Screenshots

![App Preview](assets/images/main.png)
![App Preview](assets/images/10km.png)
![App Preview](assets/images/zip_choropleth.png)
![App Preview](assets/images/int_dist.png)
![App Preview](assets/images/resposes_time.png)
![App Preview](assets/images/zip_resposes_tbl.png)

## Installation

### Requirements

In order to run the application you will need a mapbox API key.  Go to mapbox.com and create a free account.  Sign in to
your mapbox account and then create your free API key.

```bash

# Make a application directory
mkdir <app-dir>
cd <app-dir>
    
# Clone the application repository
git clone https:/github.com/MickTheLinuxGeek/Earthquake22.git
cd eartquake22
    
# Create a python virtual environment
python3 -m venv <your-env-name>

# Activate <your-env-name>
source <your-env-name>/bin/activate

# Install dependencies
pip install -r ./environment/requirements.txt

# Save your mapbox API key to a file called .mapbox_token in the earthquake22 directory

# Run app
python3 main.py

# In your browser, goto localhost:8051 to access the application
```

## Support
For any application bugs or suggestions, please file an issue on GitLab or you can consider sending email to:  mick.the.linux.geek@hotmail.com.

## Roadmap
Currently, the Earthquake Data Explorer application only displays the 'Did You Feel It?', DYFI, data for the selected event.  In the near future I would like to add
the following impact data:  
    1) Display ShakeMap - ShakeMaps provide near-real-time maps of ground motion and shaking intensity following significant earthquakes.
    2) Display PAGER - The PAGER system provides fatality and economic loss impact estimates following significant earthquakes worldwide.
Along with these updates, the application's layout will most likely change also.  There is no timetable for when these app updates will occur.

## License
MIT © Michael A. Biel